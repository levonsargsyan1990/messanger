# Contributing to Project

For contributing to this project, please:
* clone the repository
* create a branch
* make changes
* submit a pull request with a valid testing
