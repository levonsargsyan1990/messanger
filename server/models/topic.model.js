import Promise from 'bluebird';
import mongoose, { Schema } from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * Topic Schema
 */
const TopicSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  subtitle: {
    type: String,
    required: true
  },
  default: {
    type: Boolean,
    default: false,
    required: true
  },
  room: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Room'
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true
  }
}, { collection: 'topics' });

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
TopicSchema.method({
});

/**
 * Statics
 */
TopicSchema.statics = {
  /**
   * Get topic
   * @param {ObjectId} id - The objectId of topic.
   * @returns {Promise<Topic, APIError>}
   */
  get(id) {
    return this.findById(id)
      .populate('room')
      .exec()
      .then((topic) => {
        if (topic) {
          return topic;
        }
        const err = new APIError('No topic found with provided Id', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List messages in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of messages to be skipped.
   * @param {number} limit - Limit number of messages to be returned.
   * @returns {Promise<Message[]>}
   */
  list({ limit = 50, roomId } = {}) {
    return this.find({ roomId: mongoose.Types.ObjectId(roomId) }) // eslint-disable-line new-cap
      .sort({ createdAt: -1 })
      .limit(+limit)
      .exec();
  }
};

/**
 * @typedef Topic
 */
export default mongoose.model('Topic', TopicSchema);
