import Promise from 'bluebird';
import mongoose, { Schema } from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * Room Schema
 */

const RoomSchema = new Schema({
  team: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Team'
  },
  title: {
    type: String,
    required: true
  },
  subtitle: {
    type: String,
    required: true
  },
  picture: {
    type: String
  },
  owner: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  participants: {
    type: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    // type: [String],
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
  }
}, { collection: 'rooms' });

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
RoomSchema.method({
});

/**
 * Statics
 */
RoomSchema.statics = {
  /**
   * Get room
   * @param {ObjectId} id - The objectId of room.
   * @returns {Promise<Room, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((room) => {
        if (room) {
          return room;
        }
        const err = new APIError('No room found with provided Id', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List rooms in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of rooms to be skipped.
   * @param {number} limit - Limit number of rooms to be returned.
   * @returns {Promise<Room[]>}
   */
  list(filter, { skip = 0, limit = 50 } = {}) {
    return this.find(filter)
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  },
};

/**
 * @typedef Room
 */
export default mongoose.model('Room', RoomSchema);
