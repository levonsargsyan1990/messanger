/**
 * Created by sunny on 5/7/17.
 */

import Promise from 'bluebird';
import mongoose, { Schema } from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * Team Schema
 */

const TeamSchema = new Schema({
  teamdomain: {
    type: String,
    required: true
  },
  picture: {
    type: String
  },
  owner: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  participants: {
    type: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  }
}, { collection: 'teams' });

/**
 * Methods
 */
TeamSchema.method({
});

/**
 * Statics
 */
TeamSchema.statics = {
  /**
   * Get team
   * @param {ObjectId} id - The objectId of team.
   * @returns {Promise<Team, APIError>}
   */
  get(id) {
    return this.findById(id)
      .populate('owner')
      .exec()
  .then((team) => {
    if (team) {
      return team;
    }
    const err = new APIError('No team found with provided Id', httpStatus.NOT_FOUND);
    return Promise.reject(err);
  });
  },

  /**
   * Get all team
   * @returns {Promise<team[], APIError>}
   */
  getAll() {
    return this.find()
      .exec();
  }
};

/**
 * @typedef Team
 */
export default mongoose.model('Team', TeamSchema);
