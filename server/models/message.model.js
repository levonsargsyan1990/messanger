import Promise from 'bluebird';
import mongoose, { Schema } from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * Message Viewed Schema
 */
const ViewedSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true
  },
  viewedAt: {
    type: Date,
    default: Date.now
  }
}, { _id: false });


/**
 * Message Received Schema
 */
const ReceivedSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true
  },
  receivedAt: {
    type: Date,
    default: Date.now
  }
}, { _id: false });


/**
 * Message Content Schema
 */
const DataSchema = new Schema({
  content: {
    type: Object,
    required: true
  },
  type: {
    type: String,
    lowercase: true,
    enum: ['text', 'image', 'file'],
    required: true
  }
}, { _id: false });


/**
 * Message Schema
 */
const MessageSchema = new Schema({
  topic: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Topic'
  },
  data: {
    type: DataSchema
  },
  sender: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  deleted: {
    type: Boolean,
    default: false
  },
  edited: {
    type: Boolean,
    default: false
  },
  viewed: {
    type: [ViewedSchema]
  },
  received: {
    type: [ReceivedSchema]
  },
  tags: {
    type: [String],
    required: true
  },
  mentions: {
    type: [Schema.Types.ObjectId],
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true
  }
  // TODO: add meta property
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
MessageSchema.method({
});

/**
 * Statics
 */
MessageSchema.statics = {
  /**
   * Get message
   * @param {ObjectId} id - The objectId of message.
   * @returns {Promise<Message, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((message) => {
        if (message) {
          return message;
        }
        const err = new APIError('No message found with provided Id', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List messages in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of messages to be skipped.
   * @param {number} limit - Limit number of messages to be returned.
   * @returns {Promise<Message[]>}
   */
  list(filter, { skip = 0, limit = 50 } = {}) {
    return this.find(filter)
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

/**
 * @typedef Message
 */
export default mongoose.model('Message', MessageSchema);
