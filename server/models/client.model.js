import Promise from 'bluebird';
import mongoose, { Schema } from 'mongoose';
import httpStatus from 'http-status';
import crypto from 'crypto';
import APIError from '../helpers/APIError';

/**
 * Client Schema
 */

const ClientSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  refreshToken: { type: String }
}, { collection: 'clients' });

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

ClientSchema.pre('save', function (next) { // eslint-disable-line func-names
  // return;
  const client = this;
  // create a random refresh token
  client.refreshToken = crypto.randomBytes(80).toString('hex');
  return next();
});

/**
 * Methods
 */
ClientSchema.method({
});

/**
 * Statics
 */
ClientSchema.statics = {
  /**
   * Get client
   * @param {ObjectId} id - The objectId of client.
   * @returns {Promise<Client, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((client) => {
        if (client) {
          return client;
        }
        const err = new APIError('No client found with provided Id', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List clients.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<Client[]>}
   */
  list(filter, { skip = 0, limit = 50 } = {}) {
    return this.find(filter)
      .skip(+skip)
      .limit(+limit)
      .populate('user')
      .exec();
  },
};

/**
 * @typedef Client
 */
export default mongoose.model('Client', ClientSchema);
