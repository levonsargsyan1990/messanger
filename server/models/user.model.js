/* eslint-disable func-names */

import Promise from 'bluebird';
import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcryptjs';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

const SALT_WORK_FACTOR = 10; // Salt factor for password encryption

/**
 * User Position Schema
 */
const PositionSchema = new Schema({
  lng: {
    type: Number,
    required: true
  },
  lat: {
    type: Number,
    required: true
  }
});

/**
 * User Schema
 */
const UserSchema = new Schema({
  username: {
    type: String,
    required: true,
    index: { unique: true }
  },
  password: {
    type: String,
    required: true
  },
  team: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Team'
  },
  active: {
    type: Boolean,
    default: false
  },
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  mobileNumber: {
    type: String,
    required: true
  },
  position: PositionSchema,
  status: {
    type: String
  },
  picture: {
    type: String
    // TODO: consider changing to a separate Schema with uri and dimension properties
  },
  contacts: {
    type: [Schema.Types.ObjectId]
  },
  appState: {
    type: String,
    enum: ['active', 'inactive'],
    default: 'inactive'
  },
  state: {
    type: String,
    enum: ['online', 'invisible', 'offline'],
    // TODO: Add more options state options
    default: 'offline'
  },
  role: {
    type: String,
    enum: ['admin', 'user', 'superadmin'],
    default: 'user'
  },
  updatedAt: {
    type: Date,
    default: Date.now
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
}, { collection: 'users' });

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

UserSchema.pre('save', function (next) { // eslint-disable-line func-names
  // return;
  const user = this;
  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) {
    return next();
  }
  // generate a salt
  return bcrypt.genSalt(SALT_WORK_FACTOR, (saltError, salt) => {
    if (saltError) {
      return next(saltError);
    }
    // hash the password along with our new salt
    return bcrypt.hash(user.password, salt, (hashError, hash) => {
      if (hashError) {
        return next(hashError);
      }
      // override the cleartext password with the hashed one
      user.password = hash;
      return next();
    });
  });
});

/**
 * Methods
 */
UserSchema.method({
});

/**
 * Statics
 */
UserSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list(filter, { skip = 0, limit = 50 } = {}) {
    return this.find(filter)
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  },

};

/**
 * @typedef User
 */
export default mongoose.model('User', UserSchema);
