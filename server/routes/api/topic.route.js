import express from 'express';
import validate from 'express-validation';
import { topicParamValidation } from '../../../config/param-validation';
import topicCtrl from '../../controllers/topic.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** Request authorization */
  .all(topicCtrl.roomAuthorize)

  /** GET /api/topics - Get list of topics in room */
  .get(validate(topicParamValidation.getRoomTopics), topicCtrl.list)

  /** POST /api/topics - Create new topic */
  .post(validate(topicParamValidation.createTopic), topicCtrl.create);

router.route('/:topicId')
  /** Request authorization */
  .all(topicCtrl.topicAuthorize)

  /** GET /api/topics - Get the topic */
  .get(topicCtrl.get)

  /** PUT /api/topics/:topicId - Update topic */
  .put(validate(topicParamValidation.updateTopic), topicCtrl.update)

  /** DELETE /api/topics/:topicId - Delete topic */
  .delete(topicCtrl.remove);

export default router;
