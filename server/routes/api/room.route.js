import express from 'express';
import validate from 'express-validation';
import { roomParamValidation } from '../../../config/param-validation';
import roomCtrl from '../../controllers/room.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/rooms - Get list of rooms */
  .get(roomCtrl.list)

  /** POST /api/rooms - Create new room */
  .post(validate(roomParamValidation.createRoom), roomCtrl.create);

router.route('/:roomId')

  /** Request authorization */
  .all(roomCtrl.roomAuthorize)

  /** GET /api/rooms - Get the room */
  .get(roomCtrl.get)

  /** PUT /api/rooms/:roomId - Update room */
  .put(validate(roomParamValidation.updateRoom), roomCtrl.update)

  /** DELETE /api/rooms/:roomId - Delete room */
  .delete(roomCtrl.remove);

export default router;
