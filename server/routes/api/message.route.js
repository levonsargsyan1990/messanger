import express from 'express';
import validate from 'express-validation';
import { messageParamValidation } from '../../../config/param-validation';
import messageCtrl from '../../controllers/message.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** Request authorization */
  .all(messageCtrl.topicAuthorize)

  /** GET /api/messages - Get list of messages in topic */
  .get(validate(messageParamValidation.getTopicMessages), messageCtrl.list)

  /** POST /api/messages - Create new message */
  .post(validate(messageParamValidation.createMessage), messageCtrl.create);

router.route('/:messageId')
  /** Request authorization */
  .all(messageCtrl.messageAuthorize)

  /** GET /api/messages - Get list of messages in topic */
  .get(messageCtrl.get)

  /** PUT /api/users/:userId - Update user */
  .put(validate(messageParamValidation.updateMessage), messageCtrl.update)

  /** DELETE /api/users/:userId - Delete user */
  .delete(messageCtrl.remove);

export default router;
