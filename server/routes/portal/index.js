import express from 'express';
// import onboardingRoutes from './onboarding.route';
import authRoutes from './auth.route';
import onboardingRoutes from '../../routes/portal/onboarding.route';
import sideMenuRoutes from '../../routes/portal/sidemenu.route';
import chatRoutes from '../../routes/portal/chat.route';
import { authorize, getRandomNumber } from '../../beans/portal/auth';

const router = express.Router(); // eslint-disable-line new-cap

// mount auth routes at /auth
router.use('/', authRoutes);

router.use(authorize);

router.get('/random-number', getRandomNumber);

// mount onboarding routes at /onboarding
router.use('/onboarding', onboardingRoutes);

// mount side menu routes at /sidemenu
router.use('/sidemenu', sideMenuRoutes);

// mount chat routes at /chat
router.use('/chat', chatRoutes);

export default router;
