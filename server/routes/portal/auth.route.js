import express from 'express';
import validate from 'express-validation';
import { authParamValidation } from '../../../config/param-validation';
import auth from '../../beans/portal/auth';

const router = express.Router(); // eslint-disable-line new-cap

/** POST /api/auth/signup - Creates new user nd returns it */
router.route('/signup')
   .post(validate(authParamValidation.signup), auth.signup);

/** POST /api/auth/login - Returns token if correct username and password is provided */
router.route('/login')
  .post(validate(authParamValidation.login), auth.login);

/** POST /api/auth/refresh - Returns token if correct username and password is provided */
router.route('/refresh')
  .post(validate(authParamValidation.refresh), auth.refresh);

export default router;
