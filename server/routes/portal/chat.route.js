/**
 * Created by youssef on 07/06/17.
 */
import express from 'express';
import validate from 'express-validation';
import chat from '../../beans/portal/chat';
import { loadMessage } from '../../beans/portal/loadparams';
import { messageAuthorize } from '../../beans/portal/authorization';
import { messageParamValidation } from '../../../config/param-validation';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
/** Request authorization */
  .all(messageAuthorize)

/** GET /portal/chat - Get list of messages in topic */
  .get(validate(messageParamValidation.getTopicMessages), chat.list)

/** POST /portal/chat - Create new message */
  .post(validate(messageParamValidation.createMessage), chat.create);

router.route('/:messageId')
/** Request authorization */
  .all(messageAuthorize)

/** PUT /portal/chat/:messageId - Update user */
  .put(validate(messageParamValidation.updateMessage), chat.update)

/** DELETE /portal/chat/:messageId - Delete user */
  .delete(chat.remove);

router.param('messageId', loadMessage);

export default router;
