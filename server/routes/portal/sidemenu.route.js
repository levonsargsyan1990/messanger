import express from 'express';
import { listUsers, listChannels } from '../../beans/portal/sidemenu';
import { loadTeam } from '../../beans/portal/loadparams';

const router = express.Router(); // eslint-disable-line new-cap

/** POST /api/auth/signup - Creates new user nd returns it */
router.route('/team/:teamId/channels')
   .get(listChannels);

/** POST /api/auth/login - Returns token if correct username and password is provided */
router.route('/team/:teamId/users')
  .get(listUsers);

router.param('teamId', loadTeam);

export default router;
