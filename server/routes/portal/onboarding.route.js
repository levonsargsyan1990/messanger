/**
 * Created by sunny on 5/7/17.
 */
import express from 'express';
import validate from 'express-validation';
import { onboardingParamValidation } from '../../../config/param-validation';
import onboarding from '../../beans/portal/onboarding';
import { loadTeam, loadRoom } from '../../beans/portal/loadparams';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/:teamId/invite')
  /** POST /api/team/:teamId/invite - Invite new user to team */
  .post(validate(onboardingParamValidation.invite), onboarding.inviteNewUser);

router.route('/:teamId/rooms')
  /** POST /api/team/:teamId/rooms - Create new room on onboarding */
  .post(validate(onboardingParamValidation.createRoom), onboarding.createRoom);

/** POST /api/team/:teamId/rooms/:roomId - Room manipulation on onboarding process */
router.route('/:teamId/rooms/:roomId')
  /** DELETE /api/team/:teamId/rooms/:roomId - Delete room in onboarding process */
  .delete(validate(onboardingParamValidation.removeRoom), onboarding.removeRoom)
  /** UPDATE /api/team/:teamId/rooms/:roomId - Update room settings in onboarding process */
  .put(validate(onboardingParamValidation.updateRoom), onboarding.updateRoom);

router.route('/:teamId/:email')
  /** POST /api/team/:teamId/:email - Accept invitation from team */
  .post(validate(onboardingParamValidation.acceptInvite), onboarding.acceptInvitation);

router.route('/:teamId')
  /** GET /api/team/ */
  .get(validate(onboardingParamValidation.getTeam), onboarding.getTeam)
  /** UPDATE /api/team - Update current team info */
  .put(validate(onboardingParamValidation.updateTeam), onboarding.updateTeam)
  /** DELETE /api/team - Delete current team */
  .delete(onboarding.removeTeam);

router.route('/')
  /** POST /api/team - Create new team */
  .post(validate(onboardingParamValidation.createTeam), onboarding.createTeam);

router.param('teamId', loadTeam);
router.param('roomId', loadRoom);

export default router;
