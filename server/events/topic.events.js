import { topicEventEmitter } from '../controllers/topic.controller.js';

export default () => {
  const { io } = global;
  topicEventEmitter
    .on('topic.new', (topic) => {
      io.in(`room.${topic.roomId}`).emit('topic.new', topic);
    })

    .on('topic.update', (topic) => {
      io.in(`room.${topic.roomId}`).emit('topic.update', topic);
    })

    .on('topic.delete', (topic) => {
      io.in(`room.${topic.roomId}`).emit('topic.delete', topic);
    });
};
