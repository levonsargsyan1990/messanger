import { messageEventEmitter } from '../controllers/message.controller.js';

export default () => {
  const { io } = global;
  messageEventEmitter
    .on('message.new', (message) => {
      io.in(`topic.${message.topicId}`).emit('message.new', message);
    })

    .on('message.update', (message) => {
      io.in(`topic.${message.topicId}`).emit('message.update', message);
    })

    .on('message.delete', (message) => {
      io.in(`topic.${message.topicId}`).emit('message.delete', message);
    });
};
