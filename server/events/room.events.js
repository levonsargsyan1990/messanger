import { roomEventEmitter } from '../controllers/room.controller.js';

export default () => {
  const { io } = global;
  roomEventEmitter
    .on('room.new', (room) => {
      const users = room.participants.concat(room.owner);
      users.forEach((id) => {
        io.in(`user.${id}`).emit('room.new', room);
      });
    })

    .on('room.update', (room) => {
      io.in(`topic.${room._id}`).emit('room.new', room);
    })

    .on('room.delete', (room) => {
      io.in(`topic.${room._id}`).emit('room.new', room);
    });
};
