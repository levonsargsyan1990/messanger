import messageEvents from './message.events.js';
import roomEvents from './room.events.js';
import topicEvents from './topic.events.js';

export default {
  init() {
    messageEvents();
    roomEvents();
    topicEvents();
  },
  stop() {}
};
