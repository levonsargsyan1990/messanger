/*
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import uuid from 'uuid';
import { authorization, team } from './config';
import app from '../../index';

chai.config.includeStack = true;

const token = authorization;

let user = {
  username: uuid.v4(),
  teamId: team,
  mobileNumber: '1234567890',
  firstName: 'john',
  lastName: 'doe',
  password: 'abc123efg345',
  email: 'john@doe.com'
};

describe('## User APIs', () => {
  describe('# POST /api/users', () => {
    it('should create a new user', (done) => {
      request(app)
        .post('/api/users')
        .set('authorization', `Bearer ${token}`)
        .send(user)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.username).to.equal(user.username);
          expect(res.body.teamId).to.equal(user.teamId);
          expect(res.body.mobileNumber).to.equal(user.mobileNumber);
          expect(res.body.firstName).to.equal(user.firstName);
          expect(res.body.lastName).to.equal(user.lastName);
          user = res.body;
          done();
        })
        .catch(done);
    });
  });

  describe('# GET /api/users/:userId', () => {
    it('should get user details', (done) => {
      request(app)
        .get(`/api/users/${user._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.username).to.equal(user.username);
          expect(res.body.mobileNumber).to.equal(user.mobileNumber);
          done();
        })
        .catch(done);
    });

    it('should report error with message - Not found, when user does not exists', (done) => {
      request(app)
        .get('/api/users/5917393264aac7f791458cce')
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.NOT_FOUND)
        .then((res) => {
          expect(res.body.message).to.equal('Not Found');
          done();
        })
        .catch(done);
    });
  });

  describe('# PUT /api/users/:userId', () => {
    it('should update user details', (done) => {
      user.username = uuid.v4();
      request(app)
        .put(`/api/users/${user._id}`)
        .set('authorization', `Bearer ${token}`)
        .send(user)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.username).to.equal(user.username);
          expect(res.body.mobileNumber).to.equal(user.mobileNumber);
          expect(res.body.firstName).to.equal(user.firstName);
          expect(res.body.lastName).to.equal(user.lastName);
          user = res.body;
          done();
        })
        .catch(done);
    });
  });

  describe('# GET /api/users/', () => {
    it('should get all users', (done) => {
      request(app)
        .get('/api/users')
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('array');
          done();
        })
        .catch(done);
    });

    it('should get all users (with limit and skip)', (done) => {
      request(app)
        .get('/api/users')
        .query({ limit: 10, skip: 1 })
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('array');
          done();
        })
        .catch(done);
    });
  });
});

describe('# DELETE /api/users/', () => {
  it('should delete user', (done) => {
    request(app)
      .delete(`/api/users/${user._id}`)
      .set('authorization', `Bearer ${token}`)
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body.username).to.equal(user.username);
        expect(res.body.mobileNumber).to.equal(user.mobileNumber);
        done();
      })
      .catch(done);
  });
});
*/
