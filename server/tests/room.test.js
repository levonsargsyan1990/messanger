/*
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import { authorization, user, team } from './config';
import app from '../../index';

chai.config.includeStack = true;

const token = authorization;
let room = {
  title: 'Test room',
  subtitle: 'Test room subtitle',
  picture: 'https://www.ibiblio.org/wm/paint/auth/kandinsky/kandinsky.comp-8.jpg',
  owner: user,
  participants: [user]
};

describe('## Room APIs', () => {
  describe('# POST /api/rooms', () => {
    it('should create a new room', (done) => {
      request(app)
        .post(`/api/team/${team}/rooms/`)
        .set('authorization', `Bearer ${token}`)
        .send(room)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.title).to.equal(room.title);
          expect(res.body.subtitle).to.equal(room.subtitle);
          expect(res.body.picture).to.equal(room.picture);
          expect(res.body.owner).to.equal(user);
          expect(res.body.participants).to.deep.equal(room.participants);
          room = res.body;
          done();
        })
        .catch(done);
    });

    it('should report error with room - Bad token, when user is not authorized', (done) => {
      request(app)
        .post('/api/rooms')
        .send(room)
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done())
        .catch(done);
    });
  });

  describe('# GET /api/rooms/:roomId', () => {
    it('should get room details', (done) => {
      request(app)
        .get(`/api/rooms/${room._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.title).to.equal(room.title);
          expect(res.body.subtitle).to.equal(room.subtitle);
          expect(res.body.picture).to.equal(room.picture);
          expect(res.body.owner).to.equal(user);
          expect(res.body.participants).to.deep.equal(room.participants);
          room = res.body;
          done();
        })
        .catch(done);
    });

    it('should report error with room - Not found, when room does not exists', (done) => {
      request(app)
        .get('/api/rooms/56c787ccc67fc16ccc1a5e92')
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.NOT_FOUND)
        .then(() => done())
        .catch(done);
    });
  });

  describe('# PUT /api/rooms/:roomId', () => {
    it('should update message details', (done) => {
      room.title = 'Updated room';
      room.subtitle = 'Updated room subtitle';
      room.picture = 'https://www.blablabla.org';
      room.participants = ['58fd3a688f5ee95eaa1dd649', '58fd3a688f5ee95eaa1dd658'];
      request(app)
        .put(`/api/rooms/${room._id}`)
        .set('authorization', `Bearer ${token}`)
        .send(room)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.title).to.equal(room.title);
          expect(res.body.subtitle).to.equal(room.subtitle);
          expect(res.body.picture).to.equal(room.picture);
          expect(res.body.participants).to.deep.equal(room.participants);
          room = res.body;
          done();
        })
        .catch(done);
    });
  });

  describe('# DELETE /api/rooms/', () => {
    it('should delete the room', (done) => {
      request(app)
        .delete(`/api/rooms/${room._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.title).to.equal(room.title);
          expect(res.body.subtitle).to.equal(room.subtitle);
          expect(res.body.picture).to.equal(room.picture);
          expect(res.body.owner).to.equal(user);
          expect(res.body.participants).to.deep.equal(room.participants);
          done();
        })
        .catch(done);
    });
  });
});
*/
