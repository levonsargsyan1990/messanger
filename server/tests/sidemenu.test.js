/**
 * Created by abdelghafour on 6/5/17.
 */

import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import { authorization, team } from './config';
import app from '../../index';

chai.config.includeStack = true;

const token = authorization;
const teamId = team;

describe('## Side Menu', () => {
  describe('# GET /portal/sidemenu/team/:teamId/users', () => {
    it('should return users in the same user\'s team', (done) => {
      request(app)
        .get(`/portal/sidemenu/team/${teamId}/users`)
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          for (let i = 0; i < res.body.length; i += 1) {
            expect(res.body[i].username).to.be.a('String');
            expect(res.body[i].team).to.be.a('String');
            expect(res.body[i].mobileNumber).to.be.a('String');
            expect(res.body[i].firstName).to.be.a('String');
            expect(res.body[i].lastName).to.be.a('String');
          }
          done();
        })
        .catch(done);
    });
  });

  describe('# GET /portal/sidemenu/team/:teamId/channels', () => {
    it('should return rooms in the same user\'s team', (done) => {
      request(app)
        .get(`/portal/sidemenu/team/${teamId}/channels`)
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          for (let i = 0; i < res.body.length; i += 1) {
            expect(res.body[i].title).to.be.a('String');
            expect(res.body[i].subtitle).to.be.a('String');
            expect(res.body[i].owner).to.be.a('String');
            expect(res.body[i].participants).to.be.a('Array');
          }
          done();
        })
        .catch(done);
    });
  });
});

