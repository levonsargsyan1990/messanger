/**
 * Created by youssef on 08/06/17.
 */
import mongoose from 'mongoose';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import { expect } from 'chai';
import { authorization, user, topic } from './config';
import app from '../../index';

const token = authorization;

let message = {
  topicId: topic,
  sender: user,
  mentions: [
    '58fd3a688f5ee95eaa1dd641',
    '58fd3a688f5ee95eaa1dd651'
  ],
  tags: ['some tag'],
  received: [],
  viewed: [],
  edited: false,
  deleted: false,
  data: {
    content: 'Message test',
    type: 'text'
  }
};

describe('## Chat APIs', () => {
  describe('# POST /portal/chat', () => {
    it('should create a new message', (done) => {
      request(app)
        .post('/portal/chat')
        .set('authorization', `Bearer ${token}`)
        .send(message)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.topic).to.equal(message.topicId);
          expect(res.body.sender).to.equal(message.sender);
          expect(res.body.mentions).to.deep.equal(message.mentions);
          expect(res.body.tags).to.deep.equal(message.tags);
          expect(res.body.received).to.deep.equal(message.received);
          expect(res.body.viewed).to.deep.equal(message.viewed);
          expect(res.body.edited).to.equal(message.edited);
          expect(res.body.deleted).to.equal(message.deleted);
          expect(res.body.data.content).to.equal(message.data.content);
          expect(res.body.data.type).to.equal(message.data.type);
          message = res.body;
          done();
        })
        .catch(done);
    });

    it('should report error with message - Bad token, when user is not authorized', (done) => {
      request(app)
        .post('/portal/chat')
        .send(message)
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done())
        .catch(done);
    });
  });

  describe('# GET /portal/chat', () => {
    it('should get all messages', (done) => {
      request(app)
        .get(`/portal/chat?topicId=${topic}&skip=0&limit=10`)
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          for (let i = 0; i < res.body.length; i += 1) {
            expect(res.body[i].topic).to.equal(topic);
            expect(res.body[i].sender).to.be.a('String');
            expect(res.body[i].mentions).to.be.a('Array');
            expect(res.body[i].tags).to.be.a('Array');
            expect(res.body[i].received).to.be.a('Array');
            expect(res.body[i].viewed).to.be.a('Array');
            expect(res.body[i].edited).to.be.a('Boolean');
            expect(res.body[i].deleted).to.be.a('Boolean');
            expect(res.body[i].data.content).be.a('String');
            expect(res.body[i].data.type).to.be.a('String');
            done();
          }
        })
        .catch(done);
    });

    it('should report error with message - Bad token, when user is not authorized', (done) => {
      request(app)
        .get(`/portal/chat?topicId=${topic}&skip=0&limit=10`)
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done())
        .catch(done);
    });
  });

  describe('# PUT /portal/chat/:messageId', () => {
    it('should update message details', (done) => {
      message.topicId = new mongoose.Types.ObjectId(topic);
      message.mentions = [
        '58fd3a688f5ee95eaa1dd641',
        '58fd3a688f5ee95eaa1dd651',
      ];
      message.tags = ['some tag'];
      message.received = [];
      message.viewed = [];
      message.data = {
        content: 'Updated message test',
        type: 'text'
      };
      request(app)
        .put(`/portal/chat/${message._id}`)
        .set('authorization', `Bearer ${token}`)
        .send(message)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.sender).to.equal(message.sender);
          expect(res.body.mentions).to.deep.equal(message.mentions);
          expect(res.body.tags).to.deep.equal(message.tags);
          expect(res.body.received.map(u => u.userId)).to.deep.equal(message.received);
          expect(res.body.viewed.map(u => u.userId)).to.deep.equal(message.viewed);
          expect(res.body.edited).to.equal(true);
          message.edited = true;
          expect(res.body.deleted).to.equal(message.deleted);
          expect(res.body.data.content).to.equal(message.data.content);
          expect(res.body.data.type).to.equal(message.data.type);
          done();
        })
        .catch(done);
    });

    it('should report error with message - Bad token, when user is not authorized', (done) => {
      request(app)
        .put(`/portal/chat/${message._id}`)
        .send(message)
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done())
        .catch(done);
    });
  });

  describe('# DELETE /portal/chat/', () => {
    it('should delete the message', (done) => {
      request(app)
        .delete(`/portal/chat/${message._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.sender).to.equal(message.sender);
          expect(res.body.mentions).to.deep.equal(message.mentions);
          expect(res.body.tags).to.deep.equal(message.tags);
          expect(res.body.received.map(u => u.userId)).to.deep.equal(message.received);
          expect(res.body.viewed.map(u => u.userId)).to.deep.equal(message.viewed);
          expect(res.body.edited).to.equal(message.edited);
          expect(res.body.deleted).to.equal(true);
          expect(res.body.data.content).to.equal(message.data.content);
          expect(res.body.data.type).to.equal(message.data.type);
          done();
        })
        .catch(done);
    });

    it('should report error with message - Bad token, when user is not authorized', (done) => {
      request(app)
        .delete(`/portal/chat/${message._id}`)
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done())
        .catch(done);
    });
  });
});
