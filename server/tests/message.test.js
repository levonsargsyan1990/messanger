/* eslint-disable new-cap */
/*
import mongoose from 'mongoose';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import { authorization, user, topic } from './config';
import app from '../../index';

chai.config.includeStack = true;

const token = authorization;

let message = {
  topicId: topic,
  sender: user,
  mentions: [
    '58fd3a688f5ee95eaa1dd641',
    '58fd3a688f5ee95eaa1dd651'
  ],
  tags: ['some tag'],
  received: [],
  viewed: [],
  edited: false,
  deleted: false,
  types: ['text']
};

describe('## Message APIs', () => {
  describe('# POST /api/messages', () => {
    it('should create a new message', (done) => {
      request(app)
        .post('/api/messages')
        .set('authorization', `Bearer ${token}`)
        .send(message)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.topicId).to.equal(message.topicId);
          expect(res.body.sender).to.equal(message.sender);
          expect(res.body.mentions)
            .to.deep.equal(message.mentions);
          expect(res.body.tags).to.deep.equal(message.tags);
          expect(res.body.received).to.deep.equal(message.received);
          expect(res.body.viewed).to.deep.equal(message.viewed);
          expect(res.body.edited).to.equal(message.edited);
          expect(res.body.deleted).to.equal(message.deleted);
          expect(res.body.types).to.deep.equal(message.types);
          message = res.body;
          done();
        })
        .catch(done);
    });

    it('should report error with message - Bad token, when user is not authorized', (done) => {
      request(app)
        .post('/api/messages')
        .send(message)
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done())
        .catch(done);
    });
  });

  describe('# GET /api/messages/:messageId', () => {
    it('should get message details', (done) => {
      request(app)
        .get(`/api/messages/${message._id}`)
        .set('authorization', `Bearer ${token}`)
        // .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.topicId.toString()).to.equal(message.topicId.toString());
          expect(res.body.sender).to.equal(message.sender);
          expect(res.body.mentions)
            .to.deep.equal(message.mentions);
          expect(res.body.tags).to.deep.equal(message.tags);
          expect(res.body.received).to.deep.equal(message.received);
          expect(res.body.viewed).to.deep.equal(message.viewed);
          expect(res.body.edited).to.equal(message.edited);
          expect(res.body.deleted).to.equal(message.deleted);
          expect(res.body.types).to.deep.equal(message.types);
          message = res.body;
          done();
        })
        .catch(done);
    });

    it('should report error with message - Not found, when message does not exists', (done) => {
      request(app)
        .get('/api/messages/56c787ccc67fc16ccc1a5e92')
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.NOT_FOUND)
        .then((res) => {
          expect(res.body.message).to.equal('Not Found');
          done();
        })
        .catch(done);
    });
  });

  describe('# PUT /api/messages/:messageId', () => {
    it('should update message details', (done) => {
      message.topicId = mongoose.Types.ObjectId(topic);
      message.mentions = [
        '58fd3a688f5ee95eaa1dd641',
        '58fd3a688f5ee95eaa1dd651',
        '58fd3a688f5ee95eaa1dd661'
      ];
      message.tags = ['war', 'is', 'peace'];
      message.received = [
        user,
        user
      ];
      message.viewed = [
        user
      ];
      message.edited = true;
      message.deleted = true;
      message.types = ['text', 'image'];
      request(app)
        .put(`/api/messages/${message._id}`)
        .set('authorization', `Bearer ${token}`)
        .send(message)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.sender).to.equal(message.sender);
          expect(res.body.mentions).to.deep.equal(message.mentions);
          expect(res.body.tags).to.deep.equal(message.tags);
          expect(res.body.received.map(u => u.userId)).to.deep.equal(message.received);
          expect(res.body.viewed.map(u => u.userId)).to.deep.equal(message.viewed);
          expect(res.body.edited).to.equal(message.edited);
          expect(res.body.deleted).to.equal(message.deleted);
          expect(res.body.types).to.deep.equal(message.types);
          done();
        })
        .catch(done);
    });
  });
});

describe('# DELETE /api/messages/', () => {
  it('should delete the message', (done) => {
    request(app)
      .delete(`/api/messages/${message._id}`)
      .set('authorization', `Bearer ${token}`)
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body.sender).to.equal(message.sender.toString());
        expect(res.body.mentions)
          .to.deep.equal(message.mentions.map(mention => mention.toString()));
        expect(res.body.tags).to.deep.equal(message.tags);
        expect(res.body.received.map(({ userId }) => userId))
          .to.deep.equal(message.received.map(u => u.toString()));
        expect(res.body.viewed.map(({ userId }) => userId))
          .to.deep.equal(message.viewed.map(u => u.toString()));
        expect(res.body.edited).to.equal(message.edited);
        expect(res.body.deleted).to.equal(message.deleted);
        expect(res.body.types).to.deep.equal(message.types);
        done();
      })
      .catch(done);
  });
});
*/
