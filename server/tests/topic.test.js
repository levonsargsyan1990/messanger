/* eslint-disable new-cap */
/*
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import { authorization, room } from './config';
import app from '../../index';

chai.config.includeStack = true;

let topic = {
  title: 'Test topic',
  subtitle: 'Test topic subtitle',
  roomId: room,
};

const token = authorization;

describe('## Topic APIs', () => {
  describe('# POST /api/topics', () => {
    it('should create a new topic', (done) => {
      request(app)
        .post('/api/topics')
        .set('authorization', `Bearer ${token}`)
        .send(topic)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.title).to.equal(topic.title);
          expect(res.body.subtitle).to.equal(topic.subtitle);
          expect(res.body.roomId).to.equal(topic.roomId);
          topic = res.body;
          done();
        })
        .catch(done);
    });

    it('should report error with room - Bad token, when user is not authorized', (done) => {
      request(app)
        .post('/api/topics')
        .send(topic)
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done())
        .catch(done);
    });
  });

  describe('# GET /api/topics/:topicId', () => {
    it('should get topic details', (done) => {
      request(app)
        .get(`/api/topics/${topic._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.title).to.equal(topic.title);
          expect(res.body.subtitle).to.equal(topic.subtitle);
          expect(res.body.roomId).to.equal(topic.roomId);
          topic = res.body;
          done();
        })
        .catch(done);
    });

    it('should report error with topic - Not found, when topic does not exists', (done) => {
      request(app)
        .get('/api/topics/56c787ccc67fc16ccc1a5e92')
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.NOT_FOUND)
        .then(() => done())
        .catch(done);
    });
  });

  describe('# PUT /api/topics/:topicId', () => {
    it('should update topic details', (done) => {
      topic.title = 'Updated topic';
      topic.subtitle = 'Updated topic subtitle';
      request(app)
        .put(`/api/topics/${topic._id}`)
        .set('authorization', `Bearer ${token}`)
        .send(topic)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.title).to.equal(topic.title);
          expect(res.body.subtitle).to.equal(topic.subtitle);
          expect(res.body.roomId).to.equal(topic.roomId);
          topic = res.body;
          done();
        })
        .catch(done);
    });
  });

  describe('# DELETE /api/topics/', () => {
    it('should delete the topic', (done) => {
      request(app)
        .delete(`/api/topics/${topic._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.title).to.equal(topic.title);
          expect(res.body.subtitle).to.equal(topic.subtitle);
          expect(res.body.roomId).to.equal(topic.roomId);
          done();
        })
        .catch(done);
    });
  });
});
*/
