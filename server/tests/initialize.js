/* eslint-disable new-cap */
/* eslint-disable no-console */
import request from 'request';
import mongoose from 'mongoose';
import mongodb from 'mongodb';
import fs from 'fs';

const MongoClient = mongodb.MongoClient;
const dbUrl = 'mongodb://localhost:27017/inturnal-test';
const url = 'http://127.0.0.1:4040/portal/login';
const credentials = {
  username: 'KK123',
  password: '123'
};
const method = 'POST';
const optionsUser = {
  url,
  method,
  json: credentials,
};

const user = {
  _id: mongoose.Types.ObjectId('5917393264aac7f791458ccb'),
  username: 'KK123',
  mobileNumber: '1234567890',
  firstName: 'john',
  lastName: 'doe',
  password: '$2a$10$PxwQTaChF/7AocbGggV1Re6yGLTqI9glqAGMRFgu8loqu7umfE2rS',
  email: 'john@does.com'
};

const room = {
  _id: mongoose.Types.ObjectId('5917393264aac7f791458cce'),
  title: 'test room',
  subtitle: 'test room subtitle',
  owner: '5917393264aac7f791458ccb',
  team: mongoose.Types.ObjectId('5921aa9c3b22941479968792'),
  participants: []
};

const topic = {
  _id: mongoose.Types.ObjectId('5917393264aac7f791458cca'),
  title: 'test topic',
  subtitle: 'test topic subtitle',
  default: false,
  room: '5917393264aac7f791458cce',
};

const team = {
  _id: mongoose.Types.ObjectId('5921aa9c3b22941479968792'),
  owner: user._id,
  teamdomain: 'inturnall.com'
};

MongoClient.connect(dbUrl, (err, db) => {
  db.collection('users').drop();
  db.collection('rooms').drop();
  db.collection('topics').drop();
  db.collection('messages').drop();
  db.collection('teams').drop();
  const User = db.collection('users');
  const Room = db.collection('rooms');
  const Topic = db.collection('topics');
  const Team = db.collection('teams');
  User.insertMany([user])
    .then(Room.insertMany([room]), e => console.log('error while adding the user', e))
    .then(Topic.insertMany([topic]), e => console.log('error while adding the room', e))
    .then(Team.insertMany([team]), e => console.log('error while adding the topic', e))
    .then(() => {
      request(optionsUser, (error, response, body) => {
        if (error) {
          if (error.erno === 'ECONNREFUSED') {
            console.log('Unable to connect to the server. The app should be running from proper testing. Run `yarn start` for starting the server.');
            db.close();
          } else {
            console.error('Unable connect to server: ', error);
            db.close();
          }
        } else if (response.statusCode === 404) {
          console.error('Unable login. Please check the user credentials to match a user in the DB.', error);
          db.close();
        } else {
          const token = body.token;
          const stream = fs.createWriteStream('./server/tests/config.js');
          stream.once('open', () => {
            stream.write('const config = {\n');
            stream.write(`  authorization: '${token}',\n`);
            stream.write('  user: \'5917393264aac7f791458ccb\',\n');
            stream.write('  topic: \'5917393264aac7f791458cca\',\n');
            stream.write('  room: \'5917393264aac7f791458cce\',\n');
            stream.write('  team: \'5921aa9c3b22941479968792\',\n');
            stream.write('};\n');
            stream.write('module.exports = config;\n');
            stream.end();
            db.close();
          });
        }
      });
    }, e => console.log('error while adding the team', e))
    .catch(e => console.log(e));
});
