import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import uuid from 'uuid';
import chai, { expect } from 'chai';
import app from '../../index';
import config from '../../config/config';
import { authorization } from './config';

chai.config.includeStack = true;

describe('## Auth portals', () => {
  let refreshToken = {};
  const badRefreshToken = {
    refreshToken: '69a5b15ed959e61311dc3c12df1baff67f4e1574fd449ab9cd87a3eb4d9cb7f3c9be2d55bc73ee9e8874e72a1c5a112b4b880da6445d91c73c839e5cb22fc79d3368854aadf'
  };
  const token = authorization;
  const validUserCredentials = {
    username: 'KK123',
    password: '123'
  };

  const invalidUserCredentials = {
    username: 'react',
    password: 'IDontKnow'
  };

  const user = {
    username: uuid.v4(),
    mobileNumber: '1234567890',
    firstName: 'john',
    lastName: 'doe',
    password: '$2a$10$PxwQTaChF/7AocbGggV1Re6yGLTqI9glqAGMRFgu8loqu7umfE2rS',
    email: `${uuid.v4()}@does.com`,
    team: '5917393264aac7f791458ccf'
  };

  describe('# POST /portal/login', () => {
    it('should return Authentication error', (done) => {
      request(app)
        .post('/portal/login')
        .send(invalidUserCredentials)
        .expect(httpStatus.NOT_FOUND)
        .then(() => done())
        .catch(done);
    });

    it('should get valid JWT token', (done) => {
      request(app)
        .post('/portal/login')
        .send(validUserCredentials)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('token');
          expect(res.body).to.have.property('refreshToken');
          refreshToken = {
            refreshToken: res.body.refreshToken
          };
          jwt.verify(res.body.token, config.jwtSecret, (err, decoded) => {
            expect(err).to.not.be.ok; // eslint-disable-line no-unused-expressions
            expect(decoded.username).to.equal(validUserCredentials.username);
            done();
          });
        })
        .catch(done);
    });
  });

  describe('# POST /portal/signup', () => {
    it('should return success when doing signup', (done) => {
      request(app)
        .post('/portal/signup')
        .send(user)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.username).to.equal(user.username);
          expect(res.body.team).to.equal(user.team);
          expect(res.body.mobileNumber).to.equal(user.mobileNumber);
          expect(res.body.firstName).to.equal(user.firstName);
          expect(res.body.lastName).to.equal(user.lastName);
          done();
        })
        .catch(done);
    });

    it('should return success when doing signup with same username', (done) => {
      request(app)
        .post('/portal/signup')
        .send(user)
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done())
        .catch(done);
    });

    it('should return success when doing signup with same email', (done) => {
      user.email = `${uuid.v4()}@does.com`;
      request(app)
        .post('/portal/signup')
        .send(user)
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done())
        .catch(done);
    });
  });

  describe('# POST /portal/refresh', () => {
    it('should return error invalid refresh token', (done) => {
      request(app)
        .post('/portal/refresh')
        .send(badRefreshToken)
        .expect(httpStatus.UNAUTHORIZED)
        .then(() => {
          done();
        })
        .catch(done);
    });

    it('should get a new valid JWT token', (done) => {
      request(app)
        .post('/portal/refresh')
        .send(refreshToken)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('token');
          expect(res.body).to.have.property('refreshToken');
          jwt.verify(res.body.token, config.jwtSecret, (err, decoded) => {
            expect(err).to.not.be.ok; // eslint-disable-line no-unused-expressions
            expect(decoded.username).to.equal(validUserCredentials.username);
            done();
          });
        })
        .catch(done);
    });
  });

  describe('# GET /portal/random-number', () => {
    it('should fail to get random number because of missing Authorization', (done) => {
      request(app)
        .get('/portal/random-number')
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done())
        .catch(done);
    });

    it('should fail to get random number because of wrong token', (done) => {
      request(app)
        .get('/portal/random-number')
        .set('x-access-token', `Bearer ${token}`)
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done())
        .catch(done);
    });

    it('should get a random number', (done) => {
      request(app)
        .get('/portal/random-number')
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.num).to.be.a('number');
          done();
        })
        .catch(done);
    });
  });
});
