/**
 * Created by sunny on 5/7/17.
 */

import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import { authorization, user } from './config';
import app from '../../index';

chai.config.includeStack = true;

const token = authorization;

let team = {
  owner: user,
  teamdomain: 'private.inturnall.com'
};

const updateTeam = {
  owner: user,
  teamdomain: 'private2.inturnall.com'
};

const inviteuser = {
  email: 'ericrossi08@gmail.com'
};

let room = {
  title: 'onboarding',
  subtitle: 'general room for users',
  picture: 'https://inturnall.com/images/1.png',
  owner: user,
  participants: []
};

const newuser = {
  username: 'ericrossi08',
  password: 'password',
  mobileNumber: '+1321298108',
  firstName: 'Eric',
  lastName: 'Rossi'
};

const cryptedEmail = '0f6ccc015018a8cb604e9a9f02f10748939fe4ed8f';

describe('## Onboarding', () => {
  describe('# POST /portal/onboarding/', () => {
    it('should create a new team', (done) => {
      request(app)
        .post('/portal/onboarding/')
        .set('authorization', `Bearer ${token}`)
        .send(team)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.teamdomain).to.equal(team.teamdomain);
          expect(res.body.owner).to.equal(team.owner.toString());
          team = res.body;
          done();
        })
        .catch(done);
    });
  });

  describe('# GET /portal/onboarding/:teamId', () => {
    it('should return a team', (done) => {
      request(app)
        .get(`/portal/onboarding/${team._id}`)
        .set('authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.teamdomain).to.equal(team.teamdomain);
          expect(res.body.owner).to.be.a('Object');
          expect(res.body.owner._id).to.equal(team.owner.toString());
          done();
        })
        .catch(done);
    });
  });

  describe('# PUT /portal/onboarding/:teamId', () => {
    it('should update a team', (done) => {
      request(app)
        .put(`/portal/onboarding/${team._id}`)
        .set('authorization', `Bearer ${token}`)
        .send(updateTeam)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.teamdomain).to.equal(updateTeam.teamdomain);
          expect(res.body.owner).to.equal(updateTeam.owner.toString());
          done();
        })
        .catch(done);
    });
  });

  describe('# POST /portal/onboarding/:teamId/invite', () => {
    it('should send invite email to user', (done) => {
      request(app)
        .post(`/portal/onboarding/${team._id}/invite`)
        .set('authorization', `Bearer ${token}`)
        .send(inviteuser)
        .then((res) => {
          expect(res.body.result).to.equal('success');
          done();
        })
        .catch(done);
    });
  });

  describe('# POST /portal/onboarding/:teamId/:email', () => {
    it('should add a new user to team', (done) => {
      request(app)
        .post(`/portal/onboarding/${team._id}/${cryptedEmail}`)
        .set('authorization', `Bearer ${token}`)
        .send(newuser)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.teamdomain).to.equal(updateTeam.teamdomain);
          expect(res.body.owner).to.be.a('Object');
          expect(res.body.owner._id).to.equal(team.owner.toString());
          done();
        })
        .catch(done);
    });
  });

  describe('# POST /portal/onboarding/:teamId/rooms/', () => {
    it('should create a new room', (done) => {
      request(app)
        .post(`/portal/onboarding/${team._id}/rooms/`)
        .set('authorization', `Bearer ${token}`)
        .send(room)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.title).to.equal(room.title);
          expect(res.body.subtitle).to.equal(room.subtitle);
          expect(res.body.picture).to.equal(room.picture);
          // expect(res.body.teamId).to.equal(team._id); //TODO: add room to team
          room = res.body;
          done();
        })
        .catch(done);
    });
  });

  describe('# PUT /portal/onboarding/:teamId/rooms/:roomId', () => {
    it('should update a room', (done) => {
      room.subtitle = 'updated room subtitle';
      request(app)
        .put(`/portal/onboarding/${team._id}/rooms/${room._id}`)
        .set('authorization', `Bearer ${token}`)
        .send(room)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.title).to.equal(room.title);
          expect(res.body.subtitle).to.equal(room.subtitle);
          expect(res.body.picture).to.equal(room.picture);
          // expect(res.body.teamId).to.equal(team._id); //TODO: add room to team
          done();
        })
        .catch(done);
    });
  });
});

describe('# DELETE /portal/onboarding/:teamId/rooms/:roomId', () => {
  it('should delete a room', (done) => {
    request(app)
      .delete(`/portal/onboarding/${team._id}/rooms/${room._id}`)
      .set('authorization', `Bearer ${token}`)
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body._id).to.equal(room._id);
        // expect(res.body.teamId).to.equal(team._id); //TODO: add room to team
        done();
      })
      .catch(done);
  });
});

describe('# DELETE /portal/onboarding/:teamId', () => {
  it('should delete a team', (done) => {
    request(app)
      .delete(`/portal/onboarding/${team._id}`)
      .set('authorization', `Bearer ${token}`)
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body.teamdomain).to.equal(updateTeam.teamdomain);
        expect(res.body.owner).to.be.a('Object');
        expect(res.body.owner._id).to.equal(team.owner.toString());
        done();
      })
      .catch(done);
  });
});

