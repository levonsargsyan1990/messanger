import Client from '../models/client.model';

/**
 * Get user
 * @returns {User}
 */
function get(id) {
  return Client.findById(id);
}

/**
 * Create new user
 * @property {string} body.user - The user of the token.
 * @property {string} body.token - The ID of team of user.
 * @returns {client}
 */
function create(body) {
  const client = new Client({
    user: body.user,
  });
  return client.save();
}

/**
 * Get client list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {Client[]}
 */
function list(filter, pagination) {
  const { limit = 50, skip = 0 } = pagination;
  return Client.list(filter, { limit, skip });
}

export default { get, create, list };
