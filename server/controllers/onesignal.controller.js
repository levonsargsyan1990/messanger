/**
 * Created by youssef on 07/06/17.
 */

import request from 'request';
import config from './../../config/config';

function sendNotification(json) {
  return new Promise((resolve, reject) => {
    const method = 'POST';
    const endpoint = 'notifications';
    oneSignalAPI(endpoint, method, json)
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

function oneSignalAPI(endpoint, method, data) {
  return new Promise((resolve, reject) => {
    const url = config.oneSignal.url + endpoint;
    const header = {
      'Content-Type': 'application/json',
      Authorization: config.oneSignal.authorization
    };
    const options = {
      url,
      method,
      headers: header,
    };
    const json = data;
    if (json !== undefined && Object.keys(json).length !== 0) {
      json.app_id = config.oneSignal.appId;
      options.json = json;
    }
    request(options, (err, response, body) => {
      if (!err && response.statusCode < 400) {
        resolve(body);
      } else {
        reject(err);
      }
    });
  });
}

export default {
  sendNotification
};
