/**
 * Created by sunny on 5/7/17.
 */
import Team from '../models/team.model';

/**
 * Get team
 * @property {string} teamId - ID of the team.
 * @returns {Team}
 */
function getTeam(teamId) {
  return Team.get(teamId);
}

/**
 * Create new team
 * @property {string} body.teamdomain - The domain of team.
 * @property {User} body.owner - The owner of the team.
 * @returns {Promise<Team, Error>}
 */
function createTeam(body) {
  const team = new Team({
    teamdomain: body.teamdomain,
    owner: body.owner
  });
  return team.save();
}

/**
 * Delete team
 * @property {Team} - current Team object
 * @returns {Promise<Team, Error>}
 */
function removeTeam(team) {
  return team.remove();
}

/**
 * Update Current Team
 * @property team - currentTeam
 * @property {string} body.teamdomain - Updated team domain
 * @property {string} body.owner - Owner user object of this team
 * @property {string} body.picture - Updated picture of this team
 * @property {string} body.rooms - Updated rooms of this team
 * @property {string[]} body.participants - Updated participants of this team
 * @returns {Promise<Team, Error>}
 */
function updateTeam(teamId, body) {
  return Team.get(teamId)
    .then((team) => {
      const {
        teamdomain = team.teamdomain,
        owner = team.owner,
        picture = team.picture,
        rooms = team.rooms,
        participants = team.participants,
        updatedAt = Date.now()
      } = body;
      /* eslint-disable no-param-reassign */
      team.teamdomain = teamdomain;
      team.owner = owner;
      team.picture = picture;
      team.rooms = rooms;
      team.participants = participants;
      team.updatedAt = updatedAt;
      /* eslint-enable no-param-reassign */
      return team.save();
    });
}

export default {
  getTeam,
  createTeam,
  removeTeam,
  updateTeam,
};
