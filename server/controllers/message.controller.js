import events from 'events';
import Message from '../models/message.model';

/**
 * Creating the event emitter
 */
const messageEventEmitter = new events.EventEmitter();

/**
 * Get message
 * @returns {Message}
 */
function get(messageId) {
  return Message.get(messageId);
}

/**
 * Get message list.
 * @property {number} req.query.limit - Limit number of messages to be returned.
 * @returns {Message[]}
 */
function list(filter, pagination) {
  const { limit = 50, skip = 0 } = pagination;
  return Message.list(filter, { limit, skip });
}

/**
 * Create new message
 * @property {string} body.sender - The user.
 * @property {string} body.topicId - The ID of topic.
 * @property {string} body.types - Types of message.
 * @property {string[]} body.tags - Tags in message.
 * @property {string[]} body.mentions - IDs of users mantioned in message.
 * @returns {Message}
 */
function create(body) {
  const { topicId, data, tags = [], mentions = [], sender } = body;
  const message = new Message({
    topic: topicId,
    data,
    tags,
    mentions,
    sender
  });
  return message.save()
    .then((savedMessage) => {
      messageEventEmitter.emit('message.new', savedMessage);
      return savedMessage;
    });
}

/**
 * Update existing message
 * @property {object} oldMessage.data - Content and types of the message.
 * @property {string[]} oldMessage.tags - Tags in message.
 * @property {string[]} oldMessage.mentions - IDs of users mantioned in message.
 * @property {boolean} oldMessage.deleted - mark message as deleted message.
 * @property {object[]} oldMessage.received - IDs and date of users whoe received the message.
 * @property {object[]} oldMessage.viewed - IDs and dat of users whoe viewed the message.
 * @property {object} body.data - New content and types of the message.
 * @property {string[]} body.tags - New tags in message.
 * @property {string[]} body.mentions - New IDs of users mantioned in message.
 * @property {boolean} body.deleted - New mark message as deleted message.
 * @property {object[]} body.received - New IDs and date of users whoe received the message.
 * @property {object[]} body.viewed - New IDs and dat of users whoe viewed the message.
 * @returns {Message}
 */
function update(oldMessage, body) {
  const message = oldMessage;
  const now = new Date();
  const {
    data = message.data,
    tags = message.tags,
    mentions = message.mentions,
    received = [],
    viewed = [],
    deleted = false,
    } = body;
  message.data = data;
  message.tags = tags;
  message.mentions = mentions;
  message.viewed = viewed;
  message.received = received;
  message.deleted = deleted;
  message.edited = true;
  message.updatedAt = now;
  return message.save()
    .then((updatedMessage) => {
      messageEventEmitter.emit('message.update', updatedMessage);
      return updatedMessage;
    });
}

/**
 * Delete message.
 * @returns {Message}
 */
function remove(oldMessage) {
  const message = oldMessage;
  message.deleted = true;
  return message.save()
    .then((deletedMessage) => {
      messageEventEmitter.emit('message.delete', deletedMessage);
      return deletedMessage;
    });
}

export default {
  get,
  list,
  create,
  update,
  remove,
  messageEventEmitter
};
