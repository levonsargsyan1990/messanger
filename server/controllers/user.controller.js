import bcrypt from 'bcryptjs';
import User from '../models/user.model';

/**
 * Get user
 * @returns {User}
 */
function get(id) {
  return User.findById(id);
}

/**
 * Create new user
 * @property {string} body.username - The username of user.
 * @property {string} body.team - The ID of team of user.
 * @property {string} body.firstName - The first name of user.
 * @property {string} body.lastName - The last name of user.
 * @property {string} body.mobileNumber - The mobileNumber of user.
 * @property {string} body.email - The email address of user
 * @property {string} body.password - The password for this account
 * @returns {User}
 */
function create(body) {
  const user = new User({
    username: body.username,
    team: body.team,
    firstName: body.firstName,
    lastName: body.lastName,
    mobileNumber: body.mobileNumber,
    email: body.email,
    password: body.password
  });
  return user.save();
}

/**
 * Update existing user
 * @property {string} body.username - The username of user.
 * @property {string} body.mobileNumber - The mobileNumber of user.
 * @property {string} body.firstName - The firstName of user.
 * @property {string} body.lastName - The lastName of user.
 * @property {string} body.password - The password of user.
 * @returns {User}
 */
function update(body) {
  const user = {
    _id: body._id,
    username: body.username,
    firstName: body.firstName,
    lastName: body.lastName,
    mobileNumber: body.mobileNumber,
    email: body.email,
    password: body.password
  };
  return user.save();
}

/**
 * Get user list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {User[]}
 */
function list(filter, pagination) {
  const { limit = 50, skip = 0 } = pagination;
  return User.list(filter, { limit, skip });
}

/**
 * Compare password with existing user's one
 * @param {String} candidatePassword - The candidate password
 * @returns {Promise<Boolean, Error>}
 */
function comparePassword(user, candidatePassword) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(candidatePassword, user.password, (err, isMatch) => {
      if (err) {
        return reject(err);
      }
      return resolve(isMatch);
    });
  });
}

export default { get, create, update, list, comparePassword };
