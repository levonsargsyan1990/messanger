import events from 'events';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import Topic from '../models/topic.model';
import Room from '../models/room.model';

/**
 * Creating the event emitter
 */
const topicEventEmitter = new events.EventEmitter();

/**
 * Get Topic
 * @returns {Topic}
 */
function get(id) {
  return Topic.get(id);
}

/**
 * Get topic list.
 * @property {number} req.query.limit - Limit number of topics to be returned.
 * @returns {Topic[]}
 */
function list(req, res, next) {
  const { limit = 50, roomId } = req.query;
  Topic.list({ roomId, limit })
    .then((topics) => {
      res.json(topics);
    })
    .catch(e => next(e));
}

/**
 * Create new topic
 * @property {string} req.roomId - The ID of room.
 * @property {string} req.title - The title of topic.
 * @property {string} req.body.subtitle - The subtitle of topic.
 * @returns {Topic}
 */
function create(req, res, next) {
  const { roomId, title, subtitle } = req.body;
  Topic.find({ roomId })
    .then((rooms) => {
      let isDefault = true;
      if (rooms && rooms.length > 0) {
        isDefault = false;
      }
      const topic = new Topic({ roomId, title, subtitle, default: isDefault });
      return topic.save();
    })
    .then((savedTopic) => {
      topicEventEmitter.emit('topic.new', savedTopic);
      return res.json(savedTopic);
    })
    .catch(e => next(e));
}

/**
 * Update existing topic
 * @property {string} req.params.topicId - The ID of topic.
 * @property {string} req.title - The title of topic.
 * @property {string} req.body.subtitle - The subtitle of topic.
 * @returns {Topic}
 */
function update(req, res, next) {
  const { topicId } = req.params;
  Topic.get(topicId)
    .then((topic) => {
      const {
        title = topic.title,
        subtitle = topic.subtitle
      } = req.body;
      /* eslint-disable no-param-reassign */
      topic.title = title;
      topic.subtitle = subtitle;
      topic.updatedAt = new Date();
      /* eslint-enble no-param-reassign */
      return topic.save();
    })
    .then((updatedTopic) => {
      topicEventEmitter.emit('topic.update', updatedTopic);
      return res.json(updatedTopic);
    })
    .catch(e => next(e));
}

/**
 * Delete topic.
 * @property {string} req.params.topicId - The ID of topic.
 * @returns {Topic}
 */
function remove(req, res, next) {
  const { topicId } = req.params;
  Topic.get(topicId)
    .then(topic => topic.remove())
    .then((deletedTopic) => {
      topicEventEmitter.emit('topic.delete', deletedTopic);
      res.json(deletedTopic);
    })
    .catch(e => next(e));
}

/**
 * Autorize message listing or creation.
 * @returns {Message}
 */
function topicAuthorize(req, res, next) {
  const { topicId } = req.params;
  Topic.get(topicId)
    .then(topic => Room.get(topic.roomId))
    .then(({ owner, participants }) => {
      if (owner.toString() !== req.user._id.toString() && participants
        .map(user => user.toString()).indexOf(req.user._id.toString()) === -1) {
        throw new APIError('User not in this room', httpStatus.UNAUTHORIZED, true);
      }
      next();
    })
    .catch(e => next(e));
}

/**
 * Autorize message listing or creation.
 * @returns {Topic}
 */
function roomAuthorize(req, res, next) {
  const { roomId } = req.body;
  Room.get(roomId)
    .then(({ owner, participants }) => {
      if (owner.toString() !== req.user._id.toString() && participants
        .map(user => user.toString()).indexOf(req.user._id.toString()) === -1) {
        throw new APIError('User not in this room', httpStatus.UNAUTHORIZED, true);
      }
      next();
    })
    .catch(e => next(e));
}

export default {
  get,
  list,
  create,
  update,
  remove,
  topicAuthorize,
  roomAuthorize,
  topicEventEmitter
};
