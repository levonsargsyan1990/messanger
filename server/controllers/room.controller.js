import events from 'events';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import Room from '../models/room.model';

/**
 * Creating the event emitter
 */
const roomEventEmitter = new events.EventEmitter();

/**
 * Get Room
 * @property {string} roomId - The ID of the team.
 * @returns {Promise<Room, APIError>}
 */
function getRoom(roomId) {
  return Room.get(roomId);
}

/**
 * Get room list.
 * @property {number} query.limit - Limit number of room to be returned.
 * @returns {Promise<Room[], APIError>}
 */
function listRoom(filter, pagination) {
  const { limit = 50, skip = 0 } = pagination;
  return Room.list(filter, { limit, skip });
}

/**
 * Create new room
 * @property {string} user - current user.
 * @property {string} teamId - The ID of the team.
 * @property {string} body.title - The title of room.
 * @property {string} body.subtitle - The subtitle of room.
 * @property {string} body.picture - The URL of picture of the room.
 * @property {string[]} body.participants - IDs of participants of the room.
 * @returns {Promise<Room, APIError>}
 */
function createRoom(body, user, teamId) {
  const { title, subtitle, picture, participants } = body;
  const room = new Room({
    owner: user._id,
    team: teamId,
    title,
    subtitle,
    picture,
    participants
  });
  return new Promise((resolve, reject) => {
    room.save()
      .then((savedRoom) => {
        roomEventEmitter.emit('room.new', savedRoom);
        resolve(savedRoom);
      })
      .catch(e => reject(e));
  });
}

/**
 * Update existing Room
 * @property {string} roomId - The ID of room.
 * @property {string} body.title - The title of room.
 * @property {string} body.subtitle - The subtitle of room.
 * @property {string} body.picture - The URL of picture of the room.
 * @property {string[]} body.participants - IDs of participants of the room.
 * @returns {Promise<Room, APIError>}
 */
function updateRoom(roomId, body) {
  return new Promise((resolve, reject) => {
    Room.get(roomId)
      .then((room) => {
        const {
          title = room.title,
          subtitle = room.subtitle,
          picture = room.picture,
          participants = room.participants
          } = body;
        /* eslint-disable no-param-reassign */
        room.title = title;
        room.subtitle = subtitle;
        room.picture = picture;
        room.participants = participants;
        room.updatedAt = new Date();
        /* eslint-enable no-param-reassign */
        return room.save();
      })
      .then((updatedRoom) => {
        roomEventEmitter.emit('room.update', updatedRoom);
        resolve(updatedRoom);
      })
      .catch(e => reject(e));
  });
}

/**
 * Delete room.
 * @property {string} roomId - The ID of room.
 * @returns {Room}
 */
function removeRoom(roomId) {
  return new Promise((resolve, reject) => {
    Room.get(roomId)
      .then(room => room.remove())
      .then((deletedRoom) => {
        roomEventEmitter.emit('room.delete', deletedRoom);
        resolve(deletedRoom);
      })
      .catch(e => reject(e));
  });
}

/**
 * Autorize room listing or creation.
 * @returns {Room}
 */
function roomAuthorize(req, res, next) {
  const { roomId } = req.params;
  const userId = req.user._id.toString();
  Room.get(roomId)
    .then(({ owner, participants }) => {
      if (owner.toString() !== userId && participants.indexOf(userId) === -1) {
        throw new APIError('User not in this room', httpStatus.UNAUTHORIZED, true);
      }
      next();
    })
    .catch(e => next(e));
}

export default {
  getRoom,
  listRoom,
  createRoom,
  updateRoom,
  removeRoom,
  roomAuthorize,
  roomEventEmitter
};
