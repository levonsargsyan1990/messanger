import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../../config/config';
import User from '../models/user.model';
import Room from '../models/room.model';
import Topic from '../models/topic.model';

export default {
  init(socket, next) {
    const token = socket.handshake.query.token;
    if (!token) {
      return next(new APIError('Bad token', httpStatus.UNAUTHORIZED, true));
    }
    return jwt.verify(token, config.jwtSecret, (err, decoded) => {
      if (err || !decoded || !decoded.userId) {
        return next(new APIError('Bad token', httpStatus.UNAUTHORIZED, true));
      }
      return User.get(decoded.userId)
        .then((user) => {
          socket.join(`user.${decoded.userId}`);
          return Room.find({ participants: user._id });
        })
          // TODO: $or owner
        .then((rooms) => {
          const roomIds = rooms.map(({ _id }) => {
            socket.join(`room.${_id}`);
            return _id;
          });
          return roomIds.length > 0 ? Topic.find({ roomId: roomIds }) : [];
        })
        .then((topics) => {
          topics.forEach(({ _id }) => {
            socket.join(`topic.${_id}`);
            return _id;
          });
          next();
        })
        .catch(e => next(e));
    });
  }
};
