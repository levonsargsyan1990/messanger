import jwt from 'jsonwebtoken';
import ms from 'ms';
import httpStatus from 'http-status';
import APIError from '../../helpers/APIError';
import config from '../../../config/config';
import { list, comparePassword, get, create } from './../../controllers/user.controller';
import clientController from './../../controllers/client.controller';

const TOKEN_EXPIRATION = '1d';

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @param next
 * @returns {Promise<APIError>}
 */
function login(req, res, next) {
  // TODO: add email to query
  const { username, password } = req.body;
  const filter = { username };
  let user;
  return list(filter, {})
    .then((users) => {
      user = users[0];
      if (user) { return comparePassword(user, password); }
      return new Promise((resolve, reject) => reject({}));
    })
    .then((isMatch) => {
      if (!isMatch) {
        throw new APIError('Invalid password.', httpStatus.BAD_REQUEST, true);
      }
      const token = jwt.sign({
        userId: user._id,
        username: user.username
      }, config.jwtSecret, { expiresIn: TOKEN_EXPIRATION });
      clientController.create({ user: user._id })
        .then((result) => {
          res.json({
            token,
            expiresIn: Date.now() + ms(TOKEN_EXPIRATION),
            refreshToken: result.refreshToken
          });
        }, () => { res.status(401).send({ error: 'error to generate token' }); });
    }, () => { throw new APIError('Invalid password.', httpStatus.NOT_FOUND, true); })
    .catch(e => next(e));
}

/**
 * Find user from jwt token and add user ID to request object.
 * @returns {Promise<APIError>}
 */
function authorize(req, res, next) {
  // TODO: check id user is active
  const { authorization } = req.headers;
  if (!authorization) {
    throw new APIError('No authorization header found.', httpStatus.BAD_REQUEST, true);
  } else if (typeof authorization !== 'string' || authorization.indexOf('Bearer ') === -1) {
    throw new APIError('Bad authorization header.', httpStatus.BAD_REQUEST, true);
  }
  const token = authorization.split(' ')[1];
  if (!token) {
    throw new APIError('No token in authorization header.', httpStatus.BAD_REQUEST, true);
  }
  jwt.verify(token, config.jwtSecret, (err, decoded) => {
    if (err || !decoded || !decoded.userId) {
      return next(new APIError('Bad token', httpStatus.UNAUTHORIZED, true));
    }
    return get(decoded.userId)
      .then((user) => {
        // eslint-disable-next-line no-param-reassign
        req.user = user;
        return next();
      })
      .catch(e => next(e));
  });
}

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @param next
 * @returns {Promise<APIError>}
 */
function signup(req, res, next) {
  const { firstName, lastName, username,
    password, email, team, mobileNumber, picture } = req.body;
  let filter = { username };
  let existingUser;
  return list(filter, {})
    .then((users) => {
      existingUser = users[0];
      if (existingUser) {
        throw new APIError('User with such username already exists', httpStatus.BAD_REQUEST);
      }
      filter = { email };
      return list(filter, {});
    })
    .then((users) => {
      existingUser = users[0];
      if (existingUser) {
        throw new APIError('User with such email already exists', httpStatus.BAD_REQUEST);
      }
      return create({
        firstName,
        lastName,
        username,
        password,
        email,
        team,
        mobileNumber,
        picture
      });
    })
    .then(savedUser => res.send(savedUser))
    .catch(e => next(e));
}

function refresh(req, res) {
  const { refreshToken } = req.body;
  const pagination = {};
  clientController.list({ refreshToken }, pagination)
    .then((clients) => {
      if (clients.length === 0) { return res.status(401).send({ error: 'error to refresh token' }); }
      const client = clients[0];
      const user = client.user;
      const token = jwt.sign({
        userId: user._id,
        username: user.username
      }, config.jwtSecret, { expiresIn: TOKEN_EXPIRATION });
      return res.json({
        token,
        expiresIn: Date.now() + ms(TOKEN_EXPIRATION),
        refreshToken
      });
    }, () => res.status(401).send({ error: 'error to refresh token' }));
}

/**
 * This is a protected route. Will return random number only if jwt token is provided in header.
 * @param req
 * @param res
 * @returns {*}
 */
function getRandomNumber(req, res) {
  // req.user is assigned by jwt middleware if valid token is provided
  return res.json({
    user: req.user,
    num: Math.random() * 100
  });
}

export default { login, authorize, signup, getRandomNumber, refresh };
