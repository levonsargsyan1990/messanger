import nodemailer from 'nodemailer';
import crypto from 'crypto';
import config from '../../../config/config';

const algorithm = 'aes-256-ctr';

const privateKey = config.key.privateKey;

// create reusable transport method (opens pool of SMTP connections)
const smtpTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: config.email.username,
    pass: config.email.password
  }
});

/**
 * Decrypt password
 */
function decrypt(password) {
  const decipher = crypto.createDecipher(algorithm, privateKey);
  let dec = decipher.update(password, 'hex', 'utf8');
  dec += decipher.final('utf8');
  return dec;
}

/**
 * Encrypt password to save database
 * @param {string} password - password string
 */
function encrypt(password) {
  const cipher = crypto.createCipher(algorithm, privateKey);
  let crypted = cipher.update(password, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

/**
 * Send verification email to user
 * @param {User} user - User to recieve email
 */
function sendMailVerificationLink(to, team) {
  const from = `${team.owner.email}`;
  const subject = `Invitation to ${team.teamdomain}`;
  const mailbody = `<p>You are invited to ${team.teamdomain} team. Please click the link below.</p>` +
    `<a href="config.host/api/onboarding/${team._id}/${encrypt(to)}"></a>`;
  mail(from, to, subject, mailbody);
}

/**
 * Send password reset email
 * @param {User} user - User to receive email
 */
function sentMailForgotPassword(user) {
  const from = `${config.email.accountName} Team<${config.email.username}>`;
  const mailbody = `<p>Your ${config.email.accountName}  Account Credential</p><p>username : ${user.userName} , password : ${decrypt(user.password)}</p>`;
  mail(from, user.userName, 'Account password', mailbody);
}

function mail(from, email, subject, mailbody) {
  const mailOptions = {
    from, // sender address
    to: email, // list of receivers
    subject, // Subject line
    // text: result.price, // plaintext body
    html: mailbody // html body
  };

  smtpTransport.sendMail(mailOptions, () => {
    smtpTransport.close(); // shut down the connection pool, no more messages
  });
}


/**
 * Send account verification code message
 * @param {User} user - User to receive sms
 */
function sendSmsVerificationMessage() {}

export default {
  decrypt,
  encrypt,
  sendMailVerificationLink,
  sentMailForgotPassword,
  sendSmsVerificationMessage
};
