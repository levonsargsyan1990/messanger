/**
 * Created by youssef on 09/06/17.
 */
import httpStatus from 'http-status';
import APIError from '../../helpers/APIError';
import topicCtl from '../../controllers/topic.controller';


/**
 * Autorize message getting, updating and deleting a message.
 * @returns {Message}
 */
function messageAuthorize(req, res, next) {
  let topicId = '';
  // updating and deleting a message
  if ('message' in req && 'topic' in req.message) { topicId = req.message.topic; }
  // creating a message
  if ('topicId' in req.body) { topicId = req.body.topicId; }
  // listing messages
  if ('topicId' in req.query) { topicId = req.query.topicId; }

  topicCtl.get(topicId)
    .then((topic) => {
      const owner = topic.room.owner;
      const participants = topic.room.participants;
      const userId = req.user._id.toString();
      if (owner.toString() !== userId && participants.indexOf(userId) === -1) {
        throw new APIError('User not in this room', httpStatus.UNAUTHORIZED, true);
      }
      next();
    })
    .catch(e => next(e));
}


export default {
  messageAuthorize
};
