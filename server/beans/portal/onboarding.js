import teamCtrl from '../../controllers/team.controller';
import roomCtrl from '../../controllers/room.controller';
import userCtrl from '../../controllers/user.controller';
import { sendMailVerificationLink, decrypt } from './common';

/**
 * Create new team
 * @param req
 * @param res
 * @param next
 */
function createTeam(req, res, next) {
  const body = req.body;
  teamCtrl.createTeam(body)
    .then(team => res.json(team))
    .catch(error => next(error));
}

/**
 * Get team
 * @param req
 * @param res
 * @param next
 */
function getTeam(req, res, next) {
  const teamId = req.params.teamId;
  teamCtrl.getTeam(teamId)
    .then(team => res.json(team))
    .catch(error => next(error));
}

/**
 * Update team
 * @param req
 * @param res
 * @param next
 */
function updateTeam(req, res, next) {
  const team = req.team;
  const body = req.body;
  teamCtrl.updateTeam(team, body)
    .then(updatedTeam => res.json(updatedTeam))
    .catch(error => next(error));
}

/**
 * Remove team
 * @param req
 * @param res
 * @param next
 */
function removeTeam(req, res, next) {
  const team = req.team;
  teamCtrl.removeTeam(team)
    .then(removedTeam => res.json(removedTeam))
    .catch(error => next(error));
}

/**
 * Invite new user to the team
 * @param req
 * @param res
 * @param next
 */
function inviteNewUser(req, res) {
  const team = req.team;
  const email = req.body.email;
  sendMailVerificationLink(email, team);
  res.json({ result: 'success' });
}

/**
 * User accepte invitaion to join the team
 * @param req
 * @param res
 * @param next
 */
function acceptInvitation(req, res, next) {
  const team = req.team;
  const body = req.body;
  body.email = decrypt(req.params.email);
  body.team = req.params.teamId;
  userCtrl.create(body)
    .then((newuser) => {
      const elementToUpdate = team;
      elementToUpdate.participants.push(newuser._id);
      teamCtrl.updateTeam(team, elementToUpdate)
        .then(savedTeam => res.json(savedTeam))
        .catch(err => next(err));
    })
    .catch(err => next(err));
}

/**
 * Create new room
 * @param req
 * @param res
 * @param next
 */
function createRoom(req, res, next) {
  const body = req.body;
  const user = req.user;
  const teamId = req.params.teamId;
  roomCtrl.createRoom(body, user, teamId)
    .then(room => res.json(room))
    .catch(error => next(error));
}

/**
 * Update room
 * @param req
 * @param res
 * @param next
 */
function updateRoom(req, res, next) {
  const body = req.body;
  const roomId = req.params.roomId;
  roomCtrl.updateRoom(roomId, body)
    .then(room => res.json(room))
    .catch(error => next(error));
}

/**
 * Remove room
 * @param req
 * @param res
 * @param next
 */
function removeRoom(req, res, next) {
  const roomId = req.params.roomId;
  roomCtrl.removeRoom(roomId)
    .then(room => res.json(room))
    .catch(error => next(error));
}

export default {
  createTeam,
  getTeam,
  updateTeam,
  removeTeam,
  inviteNewUser,
  acceptInvitation,
  createRoom,
  updateRoom,
  removeRoom
};
