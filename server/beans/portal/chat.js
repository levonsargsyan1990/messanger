/**
 * Created by youssef on 07/06/17.
 */
import messageCtrl from '../../controllers/message.controller';

/**
 * List messages
 * @param req
 * @param res
 * @param next
 */
function list(req, res, next) {
  const { limit, skip, topicId } = req.query;
  const filter = {
    topic: topicId
  };
  const pagination = {
    limit,
    skip
  };
  messageCtrl.list(filter, pagination)
    .then(messages => res.json(messages))
    .catch(error => next(error));
}

/**
 * Create new message
 * @param req
 * @param res
 * @param next
 */
function create(req, res, next) {
  const body = req.body;
  body.sender = req.user._id.toString();
  messageCtrl.create(body)
    .then(message => res.json(message))
    .catch(error => next(error));
}

/**
 * Update message
 * @param req
 * @param res
 * @param next
 */
function update(req, res, next) {
  const message = req.message;
  const body = req.body;
  messageCtrl.update(message, body)
    .then(updatedMessage => res.json(updatedMessage))
    .catch(error => next(error));
}

/**
 * Remove message
 * @param req
 * @param res
 * @param next
 */
function remove(req, res, next) {
  const message = req.message;
  messageCtrl.remove(message)
    .then(removedMessage => res.json(removedMessage))
    .catch(error => next(error));
}

export default {
  create,
  list,
  update,
  remove
};
