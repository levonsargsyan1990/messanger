import teamCtrl from '../../controllers/team.controller';
import roomCtrl from '../../controllers/room.controller';
import messageCtrl from '../../controllers/message.controller';
/**
 * Load team
 * @param req
 * @param res
 * @param next
 */
function loadTeam(req, res, next, id) {
  teamCtrl.getTeam(id)
    .then((team) => {
      // eslint-disable-next-line no-param-reassign
      req.team = team;
      return next();
    })
    .catch(error => next(error));
}

/**
 * Load room
 * @param req
 * @param res
 * @param next
 */
function loadRoom(req, res, next, id) {
  roomCtrl.getRoom(id)
    .then((room) => {
      // eslint-disable-next-line no-param-reassign
      req.room = room;
      return next();
    })
    .catch(error => next(error));
}

/**
 * Load message
 * @param req
 * @param res
 * @param next
 */
function loadMessage(req, res, next, id) {
  messageCtrl.get(id)
    .then((message) => {
      // eslint-disable-next-line no-param-reassign
      req.message = message;
      return next();
    })
    .catch(error => next(error));
}

export default {
  loadTeam,
  loadRoom,
  loadMessage,
};
