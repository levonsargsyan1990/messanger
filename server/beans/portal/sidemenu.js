import roomCtrl from '../../controllers/room.controller';
import userCtrl from '../../controllers/user.controller';


/**
 * List users in users's team
 * @param req
 * @param res
 * @param next
 */
function listUsers(req, res, next) {
  const teamId = req.team._id;
  const filter = {
    team: teamId,
  };
  const pagination = {};
  userCtrl.list(filter, pagination)
    .then(users => res.json(users))
    .catch(error => next(error));
}

/**
 * List channels in users's team
 * @param req
 * @param res
 * @param next
 */
function listChannels(req, res, next) {
  const teamId = req.team._id;
  const filter = {
    team: teamId,
  };
  const pagination = {};
  roomCtrl.listRoom(filter, pagination)
    .then(rooms => res.json(rooms))
    .catch(error => next(error));
}

export default {
  listUsers,
  listChannels,
};
