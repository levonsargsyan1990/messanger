import userParamValidation from './params/user.params.js';
import authParamValidation from './params/auth.params.js';
import messageParamValidation from './params/message.params.js';
import roomParamValidation from './params/room.params.js';
import topicParamValidation from './params/topic.params.js';
import onboardingParamValidation from './params/onboarding.params.js';

export default {
  userParamValidation,
  authParamValidation,
  messageParamValidation,
  roomParamValidation,
  topicParamValidation,
  onboardingParamValidation
};
