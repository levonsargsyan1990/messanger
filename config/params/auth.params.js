import Joi from 'joi';

const authParamValidation = {

  // POST /api/auth/login
  login: {
    body: {
      username: Joi.string().required(),
      password: Joi.string().required(),
    }
  },

  // POST /api/auth/signup
  signup: {
    body: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      username: Joi.string().required(),
      password: Joi.string().required(),
      email: Joi.string().email().required(),
      team: Joi.string().required(),
      mobileNumber: Joi.string().required(),
      picture: Joi.string(),
    }
  },

  // POST /api/auth/signup
  refresh: {
    body: {
      refreshToken: Joi.string().required(),
    }
  },
};

export default authParamValidation;
