import Joi from 'joi';

const topicParamValidation = {
  // GET /api/topics?topicId
  getRoomTopics: {
    query: {
      roomId: Joi.string().hex().required(),
      limit: Joi.number().integer().min(0)
    }
  },

  // POST /api/topics
  createTopic: {
    body: {
      roomId: Joi.string().hex().required(),
      title: Joi.string().required(),
      subtitle: Joi.string().required(),
    }
  },

  // PUT /api/topics/:topicId
  updateTopic: {
    body: {
      title: Joi.string(),
      subtitle: Joi.string()
    }
  }
};

export default topicParamValidation;
