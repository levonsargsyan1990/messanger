import Joi from 'joi';

// const strongRegex = '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"';
const mediumRegex = new RegExp('^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})');

const userParamValidation = {
  // POST /api/users
  createUser: {
    body: {
      username: Joi.string().required(),
      teamId: Joi.string().hex().required(),
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      mobileNumber: Joi.string().regex(/[0-9]+/).required(),
      email: Joi.string().email().required(),
      password: Joi.string().regex(mediumRegex).required()
    }
  },

  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      username: Joi.string().required(),
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      mobileNumber: Joi.string().regex(/[0-9]+/).required(),
      email: Joi.string().email().required(),
      password: Joi.strict().required()
    },
    params: {
      userId: Joi.string().hex().required()
    }
  }
};

export default userParamValidation;
