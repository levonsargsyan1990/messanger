import Joi from 'joi';

const roomParamValidation = {
  // POST /api/rooms
  createRoom: {
    body: {
      title: Joi.string().required(),
      subtitle: Joi.string().required(),
      picture: Joi.string(),
      participants: Joi.array().items(Joi.string().hex())
    }
  },

  // PUT /api/rooms/:roomId
  updateRoom: {
    body: {
      title: Joi.string(),
      subtitle: Joi.string(),
      picture: Joi.string(),
      participants: Joi.array().items(Joi.string().hex())
    }
  }
};

export default roomParamValidation;
