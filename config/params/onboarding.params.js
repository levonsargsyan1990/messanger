/**
 * Created by sunny on 5/7/17.
 */

import Joi from 'joi';

const onboardingParamValidation = {
  createTeam: {
    body: {
      owner: Joi.string().required(),
      teamdomain: Joi.string().required()
    }
  },

  updateTeam: {
    body: {
      teamdomain: Joi.string().required(),
      owner: Joi.string().required()
    },
    params: {
      teamId: Joi.string().required()
    }
  },

  getTeam: {
    params: {
      teamId: Joi.string().required()
    }
  },
  invite: {
    body: {
      email: Joi.string().required()
    },
    params: {
      teamId: Joi.string().required()
    }
  },

  acceptInvite: {
    body: {
      username: Joi.string().required(),
      password: Joi.string().required(),
      mobileNumber: Joi.string().required(),
      firstName: Joi.string().required(),
      lastName: Joi.string().required()
    },
    params: {
      teamId: Joi.string().required()
    }
  },

  createRoom: {
    body: {
      title: Joi.string().required(),
      subtitle: Joi.string().required()
    },
    params: {
      teamId: Joi.string().required()
    }
  },

  updateRoom: {
    body: {
      title: Joi.string().required(),
      subtitle: Joi.string().required()
    },
    params: {
      teamId: Joi.string().required(),
      roomId: Joi.string().required()
    }
  },

  removeRoom: {
    params: {
      teamId: Joi.string().required(),
      roomId: Joi.string().required()
    }
  }
};

export default onboardingParamValidation;
