import Joi from 'joi';

const messageParamValidation = {
  // GET /portral/chat?topicId
  getTopicMessages: {
    query: {
      topicId: Joi.string().hex().required(),
      limit: Joi.number().integer().min(0),
      skip: Joi.number().integer().min(0)
    }
  },

  // POST /portral/chat
  createMessage: {
    body: {
      topicId: Joi.string().hex().required(),
      data: Joi.object().keys({
        content: Joi.string().required(),
        type: Joi.string().valid('text', 'image', 'file').required()
      }),
      tags: Joi.array().items(Joi.string()),
      mentions: Joi.array().items(Joi.string().hex())
    }
  },

  // PUT /portral/chat/:messageID
  updateMessage: {
    body: {
      data: Joi.object().keys({
        content: Joi.string().required(),
        type: Joi.string().valid('text', 'image', 'file').required()
      }),
      tags: Joi.array().items(Joi.string()),
      mentions: Joi.array().items(Joi.string().hex()),
      deleted: Joi.boolean(),
      viewed: Joi.array().items(Joi.string().hex()),
      received: Joi.array().items(Joi.string().hex())
    }
  }
};

export default messageParamValidation;
