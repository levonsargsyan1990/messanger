import Joi from 'joi';

// require and configure dotenv, will load vars in .env in PROCESS.ENV
require('dotenv').config();

// define validation for all the env vars
const envVarsSchema = Joi.object({
  NODE_ENV: Joi.string()
    .allow(['development', 'production', 'test', 'provision'])
    .default('development'),
  PORT: Joi.number()
    .default(4040),
  ADMIN_USERNAME: Joi.string().required()
    .description('Site administrator name'),
  ADMIN_EMAIL_ADDR: Joi.string().required()
    .description('Site administrator Gmail account name'),
  ADMIN_EMAIL_PWD: Joi.string().required()
    .description('Site administrtor Gmail password'),
  PRIVATE_KEY: Joi.string().required()
    .description('Private key for encryption'),
  MONGOOSE_DEBUG: Joi.boolean()
    .when('NODE_ENV', {
      is: Joi.string().equal('development'),
      then: Joi.boolean().default(true),
      otherwise: Joi.boolean().default(false)
    }),
  JWT_SECRET: Joi.string().required()
    .description('JWT Secret required to sign'),
  EXPIRE_TIME: Joi.string().required()
    .description('JWT refresh token expire time'),
  ONESIGNAL_HOST: Joi.string().required()
    .description('Onesignal host url'),
  ONESIGNAL_APPID: Joi.string().required()
    .description('Onesignal app id'),
  ONESIGNAL_AUTH: Joi.string().required()
    .description('Onesignal REST api key'),
  MONGO_HOST: Joi.string().required()
    .description('Mongo DB host url'),
  MONGO_PORT: Joi.number()
    .default(27017)
}).unknown()
  .required();

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema);
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  email: {
    accountName: envVars.ADMIN_USERNAME,
    username: envVars.ADMIN_EMAIL_ADDR,
    password: envVars.ADMIN_EMAIL_PWD
  },
  key: {
    privateKey: envVars.PRIVATE_KEY
  },
  mongooseDebug: envVars.MONGOOSE_DEBUG,
  jwtSecret: envVars.JWT_SECRET,
  expiresIn: envVars.EXPIRE_TIME,
  oneSignal: {
    url: envVars.ONESIGNAL_HOST,
    appId: envVars.ONESIGNAL_APPID,
    authorization: `Basic ${envVars.ONESIGNAL_AUTH}`
  },
  mongo: {
    host: envVars.MONGO_HOST,
    port: envVars.MONGO_PORT
  }
};

export default config;
